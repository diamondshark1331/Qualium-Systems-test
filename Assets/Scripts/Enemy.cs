﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    [SerializeField] private Transform transform;
 

    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(0, speed * Time.deltaTime / 2, 0);
        if (transform.position.y <= -5)
        {
           
            Destroy(gameObject);
        }
    }
}