﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject panel;
    [SerializeField] private GameObject explosionPrefab;
    

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame 
    void Update()
    {
        if (Input.touchCount > 0)
        {
           
           
           
            Touch touch = Input.GetTouch(0);
           
          
            if (touch.phase == TouchPhase.Began)
            {
               
                if (touch.position.x < Screen.width / 2 && transform.position.x > -2.5f)
                {
                    transform.position -= new Vector3(0.5f, 0, 0);
                  
                }

                if (touch.position.x > Screen.width / 2 && transform.position.x < 2.5f)
                {
                 
                    transform.position += new Vector3(0.5f, 0, 0);
                   
                }
              
            }
        }
#if UNITY_EDITOR
       
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x < Screen.width / 2 && transform.position.x > -2.5f)
            {
                transform.position -= new Vector3(0.5f, 0, 0);
                  
            }

            if (Input.mousePosition.x > Screen.width / 2 && transform.position.x < 2.5f)
            {
                 
                transform.position += new Vector3(0.5f, 0, 0);
                   
            }
        }
        
#endif
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        Enemy enemy = other.gameObject.GetComponent<Enemy>();

        if (enemy != null)
        {
            GameObject explosion =  Instantiate(explosionPrefab);
            explosion.transform.position = transform.position;
            
            Destroy(gameObject);
            panel.SetActive(true);
            Time.timeScale = 0.5f;
        }
    }
}