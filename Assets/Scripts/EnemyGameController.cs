﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class EnemyGameController : MonoBehaviour
{
    [SerializeField]private GameObject enemyPrefab;
    private float speed; 
    [SerializeField] private float spawnDelay =3;
    void Start()
    {
        speed = 2f;
        InvokeRepeating("Spawn", 0.0f, spawnDelay);
        InvokeRepeating("SpeedUp", 5, 5);
      
    }

    private void SpeedUp()
    {
        speed += 0.2f;
    }
    
    
    private void Spawn()
    {
        
      GameObject enemy=  Instantiate(enemyPrefab);
      
      
        Random random = new Random();
        enemy.transform.position= new Vector3(random.Next(-3,3),transform.position.y,0 );
        enemy.GetComponent<Enemy>().speed = speed;

    }
    
}
